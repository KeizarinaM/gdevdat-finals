﻿namespace Admin_Portal
{
    partial class Monsters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Monsters));
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearchMonster = new System.Windows.Forms.TextBox();
            this.btnFindMonster = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.dgvMonsters = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnWeapon = new System.Windows.Forms.Button();
            this.lblNavigation = new System.Windows.Forms.Label();
            this.btnUsers = new System.Windows.Forms.Button();
            this.btnScores = new System.Windows.Forms.Button();
            this.btnMonsters = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtAddGems = new System.Windows.Forms.TextBox();
            this.txtAddMovement = new System.Windows.Forms.TextBox();
            this.txtAddDeath = new System.Windows.Forms.TextBox();
            this.txtAddName = new System.Windows.Forms.TextBox();
            this.txtEditGems = new System.Windows.Forms.TextBox();
            this.txtEditMovement = new System.Windows.Forms.TextBox();
            this.txtEditDeath = new System.Windows.Forms.TextBox();
            this.txtEditName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonsters)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(598, 507);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 52);
            this.label2.TabIndex = 39;
            this.label2.Text = "*DELETING MONSTERS:\r\n1. Find the the monster you want to delete\r\n2. Highlight the" +
    " entire row of the monster\r\n3. Click the \"Delete a monster\" button\r\n";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSearchMonster
            // 
            this.txtSearchMonster.Location = new System.Drawing.Point(294, 340);
            this.txtSearchMonster.Name = "txtSearchMonster";
            this.txtSearchMonster.Size = new System.Drawing.Size(107, 20);
            this.txtSearchMonster.TabIndex = 31;
            this.txtSearchMonster.Text = "Enter Monster ID";
            // 
            // btnFindMonster
            // 
            this.btnFindMonster.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnFindMonster.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnFindMonster.Location = new System.Drawing.Point(243, 381);
            this.btnFindMonster.Name = "btnFindMonster";
            this.btnFindMonster.Size = new System.Drawing.Size(200, 42);
            this.btnFindMonster.TabIndex = 30;
            this.btnFindMonster.Text = "Find a monster";
            this.btnFindMonster.UseVisualStyleBackColor = false;
            this.btnFindMonster.Click += new System.EventHandler(this.btnFindMonster_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.InfoText;
            this.btnDelete.ForeColor = System.Drawing.SystemColors.Control;
            this.btnDelete.Location = new System.Drawing.Point(786, 443);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(147, 42);
            this.btnDelete.TabIndex = 29;
            this.btnDelete.Text = "Delete a monster";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.SystemColors.InfoText;
            this.btnEdit.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnEdit.Location = new System.Drawing.Point(271, 507);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(147, 49);
            this.btnEdit.TabIndex = 28;
            this.btnEdit.Text = "Edit a monster";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnAdd.Location = new System.Drawing.Point(271, 250);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(147, 49);
            this.btnAdd.TabIndex = 27;
            this.btnAdd.Text = "Add a monster";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnRefresh.Location = new System.Drawing.Point(519, 443);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(147, 42);
            this.btnRefresh.TabIndex = 26;
            this.btnRefresh.Text = "Refresh Table";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dgvMonsters
            // 
            this.dgvMonsters.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvMonsters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMonsters.Location = new System.Drawing.Point(519, 118);
            this.dgvMonsters.Name = "dgvMonsters";
            this.dgvMonsters.RowTemplate.Height = 24;
            this.dgvMonsters.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMonsters.Size = new System.Drawing.Size(414, 306);
            this.dgvMonsters.TabIndex = 25;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panel2.Controls.Add(this.btnWeapon);
            this.panel2.Controls.Add(this.lblNavigation);
            this.panel2.Controls.Add(this.btnUsers);
            this.panel2.Controls.Add(this.btnScores);
            this.panel2.Controls.Add(this.btnMonsters);
            this.panel2.Controls.Add(this.btnHome);
            this.panel2.Location = new System.Drawing.Point(-3, 101);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(176, 504);
            this.panel2.TabIndex = 24;
            // 
            // btnWeapon
            // 
            this.btnWeapon.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnWeapon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWeapon.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnWeapon.Location = new System.Drawing.Point(31, 311);
            this.btnWeapon.Name = "btnWeapon";
            this.btnWeapon.Size = new System.Drawing.Size(113, 34);
            this.btnWeapon.TabIndex = 50;
            this.btnWeapon.Text = "Weapon";
            this.btnWeapon.UseVisualStyleBackColor = false;
            this.btnWeapon.Click += new System.EventHandler(this.btnWeapon_Click);
            // 
            // lblNavigation
            // 
            this.lblNavigation.AutoSize = true;
            this.lblNavigation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNavigation.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblNavigation.Location = new System.Drawing.Point(26, 17);
            this.lblNavigation.Name = "lblNavigation";
            this.lblNavigation.Size = new System.Drawing.Size(83, 20);
            this.lblNavigation.TabIndex = 3;
            this.lblNavigation.Text = "Navigation";
            // 
            // btnUsers
            // 
            this.btnUsers.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsers.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnUsers.Location = new System.Drawing.Point(31, 125);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(113, 34);
            this.btnUsers.TabIndex = 5;
            this.btnUsers.Text = "Users";
            this.btnUsers.UseVisualStyleBackColor = false;
            this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
            // 
            // btnScores
            // 
            this.btnScores.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnScores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScores.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnScores.Location = new System.Drawing.Point(31, 250);
            this.btnScores.Name = "btnScores";
            this.btnScores.Size = new System.Drawing.Size(113, 34);
            this.btnScores.TabIndex = 4;
            this.btnScores.Text = "Scores";
            this.btnScores.UseVisualStyleBackColor = false;
            this.btnScores.Click += new System.EventHandler(this.btnScores_Click);
            // 
            // btnMonsters
            // 
            this.btnMonsters.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnMonsters.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMonsters.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnMonsters.Location = new System.Drawing.Point(31, 190);
            this.btnMonsters.Name = "btnMonsters";
            this.btnMonsters.Size = new System.Drawing.Size(113, 34);
            this.btnMonsters.TabIndex = 4;
            this.btnMonsters.Text = "Monsters";
            this.btnMonsters.UseVisualStyleBackColor = false;
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnHome.Location = new System.Drawing.Point(31, 64);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(113, 34);
            this.btnHome.TabIndex = 3;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Location = new System.Drawing.Point(-3, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(952, 100);
            this.panel1.TabIndex = 23;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.Location = new System.Drawing.Point(862, 52);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(74, 32);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.Location = new System.Drawing.Point(67, 37);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(318, 30);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(28, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(0, 0);
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // txtAddGems
            // 
            this.txtAddGems.Location = new System.Drawing.Point(347, 204);
            this.txtAddGems.Name = "txtAddGems";
            this.txtAddGems.Size = new System.Drawing.Size(134, 20);
            this.txtAddGems.TabIndex = 45;
            this.txtAddGems.Text = "Gems";
            // 
            // txtAddMovement
            // 
            this.txtAddMovement.Location = new System.Drawing.Point(195, 204);
            this.txtAddMovement.Name = "txtAddMovement";
            this.txtAddMovement.Size = new System.Drawing.Size(134, 20);
            this.txtAddMovement.TabIndex = 44;
            this.txtAddMovement.Text = "Movement Pattern";
            // 
            // txtAddDeath
            // 
            this.txtAddDeath.Location = new System.Drawing.Point(347, 150);
            this.txtAddDeath.Name = "txtAddDeath";
            this.txtAddDeath.Size = new System.Drawing.Size(107, 20);
            this.txtAddDeath.TabIndex = 43;
            this.txtAddDeath.Text = "Death Type";
            // 
            // txtAddName
            // 
            this.txtAddName.Location = new System.Drawing.Point(220, 150);
            this.txtAddName.Name = "txtAddName";
            this.txtAddName.Size = new System.Drawing.Size(109, 20);
            this.txtAddName.TabIndex = 42;
            this.txtAddName.Text = " Name";
            // 
            // txtEditGems
            // 
            this.txtEditGems.Location = new System.Drawing.Point(347, 468);
            this.txtEditGems.Name = "txtEditGems";
            this.txtEditGems.Size = new System.Drawing.Size(134, 20);
            this.txtEditGems.TabIndex = 49;
            this.txtEditGems.Text = "Gems";
            // 
            // txtEditMovement
            // 
            this.txtEditMovement.Location = new System.Drawing.Point(195, 468);
            this.txtEditMovement.Name = "txtEditMovement";
            this.txtEditMovement.Size = new System.Drawing.Size(134, 20);
            this.txtEditMovement.TabIndex = 48;
            this.txtEditMovement.Text = "Movement Pattern";
            // 
            // txtEditDeath
            // 
            this.txtEditDeath.Location = new System.Drawing.Point(347, 429);
            this.txtEditDeath.Name = "txtEditDeath";
            this.txtEditDeath.Size = new System.Drawing.Size(107, 20);
            this.txtEditDeath.TabIndex = 47;
            this.txtEditDeath.Text = "Death";
            // 
            // txtEditName
            // 
            this.txtEditName.Location = new System.Drawing.Point(222, 429);
            this.txtEditName.Name = "txtEditName";
            this.txtEditName.Size = new System.Drawing.Size(107, 20);
            this.txtEditName.TabIndex = 46;
            this.txtEditName.Text = "Name";
            // 
            // Monsters
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(946, 604);
            this.Controls.Add(this.txtEditGems);
            this.Controls.Add(this.txtEditMovement);
            this.Controls.Add(this.txtEditDeath);
            this.Controls.Add(this.txtEditName);
            this.Controls.Add(this.txtAddGems);
            this.Controls.Add(this.txtAddMovement);
            this.Controls.Add(this.txtAddDeath);
            this.Controls.Add(this.txtAddName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSearchMonster);
            this.Controls.Add(this.btnFindMonster);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dgvMonsters);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Monsters";
            this.Text = "Monsters";
            this.Load += new System.EventHandler(this.Monsters_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonsters)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSearchMonster;
        private System.Windows.Forms.Button btnFindMonster;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridView dgvMonsters;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblNavigation;
        private System.Windows.Forms.Button btnUsers;
        private System.Windows.Forms.Button btnScores;
        private System.Windows.Forms.Button btnMonsters;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtAddGems;
        private System.Windows.Forms.TextBox txtAddMovement;
        private System.Windows.Forms.TextBox txtAddDeath;
        private System.Windows.Forms.TextBox txtAddName;
        private System.Windows.Forms.TextBox txtEditGems;
        private System.Windows.Forms.TextBox txtEditMovement;
        private System.Windows.Forms.TextBox txtEditDeath;
        private System.Windows.Forms.TextBox txtEditName;
        private System.Windows.Forms.Button btnWeapon;
    }
}