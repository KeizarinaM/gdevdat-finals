﻿namespace Admin_Portal
{
    partial class Weapon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Weapon));
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnWeapon = new System.Windows.Forms.Button();
            this.lblNavigation = new System.Windows.Forms.Label();
            this.btnUsers = new System.Windows.Forms.Button();
            this.btnScores = new System.Windows.Forms.Button();
            this.btnMonsters = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtAddPower = new System.Windows.Forms.TextBox();
            this.txtAddFingers = new System.Windows.Forms.TextBox();
            this.txtAddGems = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.dgvWeapons = new System.Windows.Forms.DataGridView();
            this.txtEditPower = new System.Windows.Forms.TextBox();
            this.txtEditFingers = new System.Windows.Forms.TextBox();
            this.txtEditGems = new System.Windows.Forms.TextBox();
            this.txtSearchWeapon = new System.Windows.Forms.TextBox();
            this.btnFindWeapon = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWeapons)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panel2.Controls.Add(this.btnWeapon);
            this.panel2.Controls.Add(this.lblNavigation);
            this.panel2.Controls.Add(this.btnUsers);
            this.panel2.Controls.Add(this.btnScores);
            this.panel2.Controls.Add(this.btnMonsters);
            this.panel2.Controls.Add(this.btnHome);
            this.panel2.Location = new System.Drawing.Point(-3, 101);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(176, 504);
            this.panel2.TabIndex = 11;
            // 
            // btnWeapon
            // 
            this.btnWeapon.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnWeapon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWeapon.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnWeapon.Location = new System.Drawing.Point(31, 309);
            this.btnWeapon.Name = "btnWeapon";
            this.btnWeapon.Size = new System.Drawing.Size(113, 34);
            this.btnWeapon.TabIndex = 6;
            this.btnWeapon.Text = "Weapon";
            this.btnWeapon.UseVisualStyleBackColor = false;
            this.btnWeapon.Click += new System.EventHandler(this.btnWeapon_Click);
            // 
            // lblNavigation
            // 
            this.lblNavigation.AutoSize = true;
            this.lblNavigation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNavigation.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblNavigation.Location = new System.Drawing.Point(26, 17);
            this.lblNavigation.Name = "lblNavigation";
            this.lblNavigation.Size = new System.Drawing.Size(83, 20);
            this.lblNavigation.TabIndex = 3;
            this.lblNavigation.Text = "Navigation";
            // 
            // btnUsers
            // 
            this.btnUsers.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsers.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnUsers.Location = new System.Drawing.Point(31, 125);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(113, 34);
            this.btnUsers.TabIndex = 5;
            this.btnUsers.Text = "Users";
            this.btnUsers.UseVisualStyleBackColor = false;
            this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
            // 
            // btnScores
            // 
            this.btnScores.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnScores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScores.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnScores.Location = new System.Drawing.Point(31, 250);
            this.btnScores.Name = "btnScores";
            this.btnScores.Size = new System.Drawing.Size(113, 34);
            this.btnScores.TabIndex = 4;
            this.btnScores.Text = "Scores";
            this.btnScores.UseVisualStyleBackColor = false;
            this.btnScores.Click += new System.EventHandler(this.btnScores_Click);
            // 
            // btnMonsters
            // 
            this.btnMonsters.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnMonsters.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMonsters.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnMonsters.Location = new System.Drawing.Point(31, 190);
            this.btnMonsters.Name = "btnMonsters";
            this.btnMonsters.Size = new System.Drawing.Size(113, 34);
            this.btnMonsters.TabIndex = 4;
            this.btnMonsters.Text = "Monsters";
            this.btnMonsters.UseVisualStyleBackColor = false;
            this.btnMonsters.Click += new System.EventHandler(this.btnMonsters_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnHome.Location = new System.Drawing.Point(31, 64);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(113, 34);
            this.btnHome.TabIndex = 3;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Location = new System.Drawing.Point(-3, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(952, 100);
            this.panel1.TabIndex = 10;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.Location = new System.Drawing.Point(862, 52);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(74, 32);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.Location = new System.Drawing.Point(67, 37);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(318, 30);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(28, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(0, 0);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // txtAddPower
            // 
            this.txtAddPower.Location = new System.Drawing.Point(201, 226);
            this.txtAddPower.Name = "txtAddPower";
            this.txtAddPower.Size = new System.Drawing.Size(130, 20);
            this.txtAddPower.TabIndex = 17;
            this.txtAddPower.Text = "Enter Amount of Dmg";
            // 
            // txtAddFingers
            // 
            this.txtAddFingers.Location = new System.Drawing.Point(201, 186);
            this.txtAddFingers.Name = "txtAddFingers";
            this.txtAddFingers.Size = new System.Drawing.Size(152, 20);
            this.txtAddFingers.TabIndex = 16;
            this.txtAddFingers.Text = "Enter Amount of Fingers";
            // 
            // txtAddGems
            // 
            this.txtAddGems.Location = new System.Drawing.Point(201, 144);
            this.txtAddGems.Name = "txtAddGems";
            this.txtAddGems.Size = new System.Drawing.Size(134, 20);
            this.txtAddGems.TabIndex = 15;
            this.txtAddGems.Text = "Enter Gems Needed";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnAdd.Location = new System.Drawing.Point(201, 276);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(147, 49);
            this.btnAdd.TabIndex = 18;
            this.btnAdd.Text = "Add a weapon";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnEdit.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnEdit.Location = new System.Drawing.Point(201, 520);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(147, 49);
            this.btnEdit.TabIndex = 22;
            this.btnEdit.Text = "Edit a weapon";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(605, 504);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(198, 91);
            this.label2.TabIndex = 26;
            this.label2.Text = "*EDITTNG WEAPONS:\r\n1. Search Weapon\r\n2. Edit the text boxes\r\n3.. Click the \"edit " +
    "a weapon\" button\r\n*DELETING WEAPONS\r\n1. Highlight the entire row of the weapon\r\n" +
    "3. Click the \"Delete a weapon\" button\r\n";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.InfoText;
            this.btnDelete.ForeColor = System.Drawing.SystemColors.Control;
            this.btnDelete.Location = new System.Drawing.Point(766, 443);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(147, 42);
            this.btnDelete.TabIndex = 25;
            this.btnDelete.Text = "Delete a weapon";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnRefresh.Location = new System.Drawing.Point(499, 443);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(147, 42);
            this.btnRefresh.TabIndex = 24;
            this.btnRefresh.Text = "Refresh Table";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dgvWeapons
            // 
            this.dgvWeapons.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvWeapons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWeapons.Location = new System.Drawing.Point(499, 118);
            this.dgvWeapons.Name = "dgvWeapons";
            this.dgvWeapons.RowTemplate.Height = 24;
            this.dgvWeapons.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvWeapons.Size = new System.Drawing.Size(414, 306);
            this.dgvWeapons.TabIndex = 23;
            // 
            // txtEditPower
            // 
            this.txtEditPower.Location = new System.Drawing.Point(201, 473);
            this.txtEditPower.Name = "txtEditPower";
            this.txtEditPower.Size = new System.Drawing.Size(130, 20);
            this.txtEditPower.TabIndex = 29;
            this.txtEditPower.Text = "Enter Amount of Dmg";
            // 
            // txtEditFingers
            // 
            this.txtEditFingers.Location = new System.Drawing.Point(201, 433);
            this.txtEditFingers.Name = "txtEditFingers";
            this.txtEditFingers.Size = new System.Drawing.Size(134, 20);
            this.txtEditFingers.TabIndex = 28;
            this.txtEditFingers.Text = "Enter Amount of Fingers";
            // 
            // txtEditGems
            // 
            this.txtEditGems.Location = new System.Drawing.Point(201, 391);
            this.txtEditGems.Name = "txtEditGems";
            this.txtEditGems.Size = new System.Drawing.Size(134, 20);
            this.txtEditGems.TabIndex = 27;
            this.txtEditGems.Text = "Enter Gems Needed";
            // 
            // txtSearchWeapon
            // 
            this.txtSearchWeapon.Location = new System.Drawing.Point(366, 467);
            this.txtSearchWeapon.Name = "txtSearchWeapon";
            this.txtSearchWeapon.Size = new System.Drawing.Size(92, 20);
            this.txtSearchWeapon.TabIndex = 27;
            this.txtSearchWeapon.Text = "Enter Weapon ID";
            // 
            // btnFindWeapon
            // 
            this.btnFindWeapon.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnFindWeapon.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnFindWeapon.Location = new System.Drawing.Point(366, 493);
            this.btnFindWeapon.Name = "btnFindWeapon";
            this.btnFindWeapon.Size = new System.Drawing.Size(93, 33);
            this.btnFindWeapon.TabIndex = 30;
            this.btnFindWeapon.Text = "Search Weapon";
            this.btnFindWeapon.UseVisualStyleBackColor = false;
            this.btnFindWeapon.Click += new System.EventHandler(this.btnFindWeapon_Click);
            // 
            // Weapon
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(946, 604);
            this.Controls.Add(this.btnFindWeapon);
            this.Controls.Add(this.txtEditPower);
            this.Controls.Add(this.txtEditFingers);
            this.Controls.Add(this.txtSearchWeapon);
            this.Controls.Add(this.txtEditGems);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dgvWeapons);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtAddPower);
            this.Controls.Add(this.txtAddFingers);
            this.Controls.Add(this.txtAddGems);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Weapon";
            this.Text = "Weapon";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWeapons)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnWeapon;
        private System.Windows.Forms.Label lblNavigation;
        private System.Windows.Forms.Button btnUsers;
        private System.Windows.Forms.Button btnScores;
        private System.Windows.Forms.Button btnMonsters;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtAddPower;
        private System.Windows.Forms.TextBox txtAddFingers;
        private System.Windows.Forms.TextBox txtAddGems;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridView dgvWeapons;
        private System.Windows.Forms.TextBox txtEditPower;
        private System.Windows.Forms.TextBox txtEditFingers;
        private System.Windows.Forms.TextBox txtEditGems;
        private System.Windows.Forms.TextBox txtSearchWeapon;
        private System.Windows.Forms.Button btnFindWeapon;
    }
}