﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Portal
{
    public partial class Weapon : Form
    {
        public Weapon()
        {
            InitializeComponent();
        }
//------------------------NAVIGATION BUTTONS------------------------
        private void btnScores_Click(object sender, EventArgs e)
        {
            Scores scores = new Scores();
            ButtonControl.navigationButton(this, scores);
        }

        private void btnWeapon_Click(object sender, EventArgs e)
        {
            Weapon weapon = new Weapon();
            ButtonControl.navigationButton(this, weapon);
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Form1 main = new Form1();
            ButtonControl.navigationButton(this, main);
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            Users users = new Users();
            ButtonControl.navigationButton(this, users);
        }

        private void btnMonsters_Click(object sender, EventArgs e)
        {
            Monsters monsters = new Monsters();
            ButtonControl.navigationButton(this, monsters);
        }
        //------------------------OTHER BUTTONS------------------------

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;
            cmd.CommandText = "update Weapon set Gems = @gems,Fingers = @fingers, Power = @power where WeaponID = @weaponID";
            cmd.Parameters.AddWithValue("@gems", txtEditGems.Text);
            cmd.Parameters.AddWithValue("@fingers", txtEditFingers.Text);
            cmd.Parameters.AddWithValue("@power", txtEditPower.Text);
            cmd.Parameters.AddWithValue("@weaponID", txtSearchWeapon.Text);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Sucessfully Updated");
            Helper.cn.Close();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ButtonControl.refreshButton("Weapon", dgvWeapons);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;
            cmd.CommandText = "insert into Weapon values (@gems, @fingers, @power)";
            cmd.Parameters.AddWithValue("@gems", txtAddGems.Text);
            cmd.Parameters.AddWithValue("@fingers", txtAddFingers.Text);
            cmd.Parameters.AddWithValue("@power", txtAddPower.Text);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Succesfully Added");
            Helper.cn.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;

            cmd.CommandText = "delete from Weapon where WeaponID = @idno";
            cmd.Parameters.AddWithValue("@idno", dgvWeapons.SelectedRows[0].Cells[0].Value.ToString());
            cmd.ExecuteNonQuery();

            Helper.cn.Close();
            MessageBox.Show("Successfully Deleted!");
        }

        private void btnFindWeapon_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;

            if (txtSearchWeapon.Text == "Enter Weapon ID")
            {
                MessageBox.Show("No valid User ID found");
                return;
            }

            cmd.CommandText = "Select * from Weapon where WeaponID = @weaponID";
            cmd.Parameters.AddWithValue("@weaponID", txtSearchWeapon.Text);
            cmd.ExecuteNonQuery();

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvWeapons.DataSource = dt;

            cmd.ExecuteNonQuery();

            cmd.CommandText = "Select Gems from Weapon where WeaponID = @weaponID";
            string gems = (string)cmd.ExecuteScalar();
            txtEditGems.Text = gems;
            cmd.ExecuteNonQuery();

            cmd.CommandText = "Select Fingers from Weapon where WeaponID = @weaponID";
            string fingers = (string)cmd.ExecuteScalar();
            txtEditFingers.Text = fingers;
            cmd.ExecuteNonQuery();

            cmd.CommandText = "Select Power from Weapon where WeaponID = @weaponID";
            string power = (string)cmd.ExecuteScalar();
            txtEditPower.Text = power;
            cmd.ExecuteNonQuery();

            Helper.cn.Close();
        }
    }
}
