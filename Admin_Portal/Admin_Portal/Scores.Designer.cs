﻿namespace Admin_Portal
{
    partial class Scores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Scores));
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnWeapon = new System.Windows.Forms.Button();
            this.lblNavigation = new System.Windows.Forms.Label();
            this.btnUsers = new System.Windows.Forms.Button();
            this.btnScores = new System.Windows.Forms.Button();
            this.btnMonsters = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dgvScores = new System.Windows.Forms.DataGridView();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnTopScores = new System.Windows.Forms.Button();
            this.btnLowestScores = new System.Windows.Forms.Button();
            this.btnDeath = new System.Windows.Forms.Button();
            this.txtDeathType = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvScores)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panel2.Controls.Add(this.btnWeapon);
            this.panel2.Controls.Add(this.lblNavigation);
            this.panel2.Controls.Add(this.btnUsers);
            this.panel2.Controls.Add(this.btnScores);
            this.panel2.Controls.Add(this.btnMonsters);
            this.panel2.Controls.Add(this.btnHome);
            this.panel2.Location = new System.Drawing.Point(-3, 101);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(176, 504);
            this.panel2.TabIndex = 11;
            // 
            // btnWeapon
            // 
            this.btnWeapon.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnWeapon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWeapon.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnWeapon.Location = new System.Drawing.Point(31, 309);
            this.btnWeapon.Name = "btnWeapon";
            this.btnWeapon.Size = new System.Drawing.Size(113, 34);
            this.btnWeapon.TabIndex = 6;
            this.btnWeapon.Text = "Weapon";
            this.btnWeapon.UseVisualStyleBackColor = false;
            this.btnWeapon.Click += new System.EventHandler(this.btnWeapon_Click);
            // 
            // lblNavigation
            // 
            this.lblNavigation.AutoSize = true;
            this.lblNavigation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNavigation.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblNavigation.Location = new System.Drawing.Point(26, 17);
            this.lblNavigation.Name = "lblNavigation";
            this.lblNavigation.Size = new System.Drawing.Size(104, 25);
            this.lblNavigation.TabIndex = 3;
            this.lblNavigation.Text = "Navigation";
            // 
            // btnUsers
            // 
            this.btnUsers.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsers.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnUsers.Location = new System.Drawing.Point(31, 125);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(113, 34);
            this.btnUsers.TabIndex = 5;
            this.btnUsers.Text = "Users";
            this.btnUsers.UseVisualStyleBackColor = false;
            this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
            // 
            // btnScores
            // 
            this.btnScores.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnScores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScores.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnScores.Location = new System.Drawing.Point(31, 250);
            this.btnScores.Name = "btnScores";
            this.btnScores.Size = new System.Drawing.Size(113, 34);
            this.btnScores.TabIndex = 4;
            this.btnScores.Text = "Scores";
            this.btnScores.UseVisualStyleBackColor = false;
            this.btnScores.Click += new System.EventHandler(this.btnScores_Click);
            // 
            // btnMonsters
            // 
            this.btnMonsters.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnMonsters.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMonsters.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnMonsters.Location = new System.Drawing.Point(31, 190);
            this.btnMonsters.Name = "btnMonsters";
            this.btnMonsters.Size = new System.Drawing.Size(113, 34);
            this.btnMonsters.TabIndex = 4;
            this.btnMonsters.Text = "Monsters";
            this.btnMonsters.UseVisualStyleBackColor = false;
            this.btnMonsters.Click += new System.EventHandler(this.btnMonsters_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnHome.Location = new System.Drawing.Point(31, 64);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(113, 34);
            this.btnHome.TabIndex = 3;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Location = new System.Drawing.Point(-3, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(952, 100);
            this.panel1.TabIndex = 10;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.Location = new System.Drawing.Point(862, 52);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(74, 32);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.Location = new System.Drawing.Point(67, 37);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(318, 30);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(28, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(0, 0);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // dgvScores
            // 
            this.dgvScores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvScores.Location = new System.Drawing.Point(216, 118);
            this.dgvScores.Name = "dgvScores";
            this.dgvScores.RowTemplate.Height = 24;
            this.dgvScores.Size = new System.Drawing.Size(692, 285);
            this.dgvScores.TabIndex = 12;
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnRefresh.Location = new System.Drawing.Point(487, 421);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(147, 42);
            this.btnRefresh.TabIndex = 25;
            this.btnRefresh.Text = "Refresh Table";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnTopScores
            // 
            this.btnTopScores.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnTopScores.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnTopScores.Location = new System.Drawing.Point(216, 501);
            this.btnTopScores.Name = "btnTopScores";
            this.btnTopScores.Size = new System.Drawing.Size(147, 42);
            this.btnTopScores.TabIndex = 26;
            this.btnTopScores.Text = "View Top Scorers";
            this.btnTopScores.UseVisualStyleBackColor = false;
            this.btnTopScores.Click += new System.EventHandler(this.btnTopScores_Click);
            // 
            // btnLowestScores
            // 
            this.btnLowestScores.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnLowestScores.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnLowestScores.Location = new System.Drawing.Point(487, 501);
            this.btnLowestScores.Name = "btnLowestScores";
            this.btnLowestScores.Size = new System.Drawing.Size(147, 42);
            this.btnLowestScores.TabIndex = 27;
            this.btnLowestScores.Text = "View Lowest Scorers";
            this.btnLowestScores.UseVisualStyleBackColor = false;
            this.btnLowestScores.Click += new System.EventHandler(this.btnLowestScores_Click);
            // 
            // btnDeath
            // 
            this.btnDeath.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnDeath.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnDeath.Location = new System.Drawing.Point(761, 501);
            this.btnDeath.Name = "btnDeath";
            this.btnDeath.Size = new System.Drawing.Size(147, 42);
            this.btnDeath.TabIndex = 28;
            this.btnDeath.Text = "View by death";
            this.btnDeath.UseVisualStyleBackColor = false;
            this.btnDeath.Click += new System.EventHandler(this.btnDeath_Click);
            // 
            // txtDeathType
            // 
            this.txtDeathType.Location = new System.Drawing.Point(761, 466);
            this.txtDeathType.Name = "txtDeathType";
            this.txtDeathType.Size = new System.Drawing.Size(147, 22);
            this.txtDeathType.TabIndex = 29;
            this.txtDeathType.Text = "Enter Death Type";
            // 
            // Scores
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(946, 604);
            this.Controls.Add(this.txtDeathType);
            this.Controls.Add(this.btnDeath);
            this.Controls.Add(this.btnLowestScores);
            this.Controls.Add(this.btnTopScores);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dgvScores);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Scores";
            this.Text = "Scores";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvScores)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnWeapon;
        private System.Windows.Forms.Label lblNavigation;
        private System.Windows.Forms.Button btnUsers;
        private System.Windows.Forms.Button btnScores;
        private System.Windows.Forms.Button btnMonsters;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dgvScores;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnTopScores;
        private System.Windows.Forms.Button btnLowestScores;
        private System.Windows.Forms.Button btnDeath;
        private System.Windows.Forms.TextBox txtDeathType;
    }
}