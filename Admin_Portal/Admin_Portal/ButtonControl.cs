﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace Admin_Portal
{
    class ButtonControl
    {
        public static void navigationButton(Form currentForm, Form nextForm)
        {
            currentForm.Hide(); //Hide this form
            nextForm.Show(); //Show new form
        }

        public static void refreshButton(string table, DataGridView dgv )
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;
            cmd.CommandText = "select * from " + table;

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgv.DataSource = dt;
            Helper.cn.Close();
        }
    }
}
