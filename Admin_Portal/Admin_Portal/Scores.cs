﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Portal
{
    public partial class Scores : Form
    {
        public Scores()
        {
            InitializeComponent();
        }
//------------------------NAVIGATION BUTTONS------------------------
        private void btnScores_Click(object sender, EventArgs e)
        {
            Scores scores = new Scores();
            ButtonControl.navigationButton(this, scores);
        }

        private void btnWeapon_Click(object sender, EventArgs e)
        {
            Weapon weapon = new Weapon();
            ButtonControl.navigationButton(this, weapon);
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Form1 main = new Form1();
            ButtonControl.navigationButton(this, main);
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            Users users = new Users();
            ButtonControl.navigationButton(this, users);
        }

        private void btnMonsters_Click(object sender, EventArgs e)
        {
            Monsters monsters = new Monsters();
            ButtonControl.navigationButton(this, monsters);
        }
//------------------------OTHER BUTTONS------------------------
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ButtonControl.refreshButton("Players", dgvScores);
        }

        private void btnDeath_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;

            cmd.CommandText = "Select DeathID from Deaths where CauseOfDeath = @causeDeath";
            cmd.Parameters.AddWithValue("@causeDeath", txtDeathType.Text);
            int deathID = (int)cmd.ExecuteScalar();
            cmd.ExecuteNonQuery();

            cmd.CommandText = "Select UserName, HighScore from Players where DeathIDno = @deathID";
            cmd.Parameters.AddWithValue("@deathID", deathID);
            cmd.ExecuteNonQuery();

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvScores.DataSource = dt;
            Helper.cn.Close();
        }

        private void btnTopScores_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;
            cmd.CommandText = "SELECT UserName, HighScore FROM Players ORDER BY HighScore DESC";

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvScores.DataSource = dt;
            Helper.cn.Close();
        }

        private void btnLowestScores_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;
            cmd.CommandText = "SELECT UserName, HighScore FROM Players ORDER BY HighScore ASC";


            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvScores.DataSource = dt;
            Helper.cn.Close();
        }
    }
}