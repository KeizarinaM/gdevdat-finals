﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Portal
{
    public partial class Monsters : Form
    {
        public Monsters()
        {
            InitializeComponent();
        }
//------------------------NAVIGATION BUTTONS------------------------
        private void btnHome_Click(object sender, EventArgs e)
        {
            Form1 main = new Form1();
            ButtonControl.navigationButton(this, main);
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            Users users = new Users();
            ButtonControl.navigationButton(this, users);
        }

        private void btnWeapon_Click(object sender, EventArgs e)
        {
            Weapon weapon = new Weapon();
            ButtonControl.navigationButton(this, weapon);
        }

        private void btnScores_Click(object sender, EventArgs e)
        {
            Scores scores = new Scores();
            ButtonControl.navigationButton(this, scores);
        }
//------------------------OTHER BUTTONS------------------------
        private void Monsters_Load(object sender, EventArgs e)
        {

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ButtonControl.refreshButton("Monster", dgvMonsters);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;

            cmd.CommandText = "insert into Monster values (@name, @death, @gems, @movement)";
            cmd.Parameters.AddWithValue("@name", txtAddName.Text);
            cmd.Parameters.AddWithValue("@death", txtAddDeath.Text);
            cmd.Parameters.AddWithValue("@gems", txtAddGems.Text);
            cmd.Parameters.AddWithValue("@movement", txtAddMovement.Text);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Succesfully Added");
            Helper.cn.Close();
        }

        private void btnFindMonster_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;

            if (txtSearchMonster.Text == "Enter Monster ID")
            {
                MessageBox.Show("No valid User ID found");
                return;
            }

            cmd.CommandText = "Select * from Monster where MonsterID = @monsterID";
            cmd.Parameters.AddWithValue("@monsterID", txtSearchMonster.Text);
            cmd.ExecuteNonQuery();

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvMonsters.DataSource = dt;

            cmd.ExecuteNonQuery();

            cmd.CommandText = "Select Name from Monster where MonsterID = @monsterID";
            string nameMonster = (string)cmd.ExecuteScalar();
            txtEditName.Text = nameMonster;
            cmd.ExecuteNonQuery();

            cmd.CommandText = "Select MovementPattern from Monster where MonsterID = @monsterID";
            string movementPattern = (string)cmd.ExecuteScalar();
            txtEditMovement.Text = movementPattern;
            cmd.ExecuteNonQuery();

            cmd.CommandText = "Select DeathType from Monster where MonsterID = @monsterID";
            string deathType = (string)cmd.ExecuteScalar();
            txtEditDeath.Text = deathType;
            cmd.ExecuteNonQuery();

            cmd.CommandText = "Select Gems from Monster where MonsterID = @monsterID";
            string gems = (string)cmd.ExecuteScalar();
            txtEditGems.Text = gems;
            cmd.ExecuteNonQuery();

            Helper.cn.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;

            cmd.CommandText = "delete from Monster where MonsterID = @idno";
            cmd.Parameters.AddWithValue("@idno", dgvMonsters.SelectedRows[0].Cells[0].Value.ToString());
            cmd.ExecuteNonQuery();

            Helper.cn.Close();
            MessageBox.Show("Successfully Deleted!");
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;
            cmd.CommandText = "update Monster set Name = @name,DeathType = @death, MovementPattern = @movement, Gems = @gems where MonsterID = @IDno";
            cmd.Parameters.AddWithValue("@name", txtEditName.Text);
            cmd.Parameters.AddWithValue("@death", txtEditDeath.Text);
            cmd.Parameters.AddWithValue("@movement", txtEditMovement.Text);
            cmd.Parameters.AddWithValue("@gems", txtEditGems.Text);
            cmd.Parameters.AddWithValue("@IDno", txtSearchMonster.Text);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Sucessfully Updated");
            Helper.cn.Close();
        }
    }
}
