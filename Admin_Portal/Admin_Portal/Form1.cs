﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Admin_Portal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;

            cmd.CommandText = "SELECT top 15 UserName, HighScore FROM Players ORDER BY HighScore DESC";
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvTopUsers.DataSource = dt;
            Helper.cn.Close();

            Helper.openConnection();

            SqlCommand cmd2 = new SqlCommand();
            cmd2.Connection = Helper.cn;
            cmd2.CommandText = "select count(*) from Players";
            Int32 count = (Int32)cmd2.ExecuteScalar();
            NumberUser.Text = count.ToString();
            int earnedAmount = count * 165;
            lblAmountEarned.Text = "₱" + earnedAmount.ToString();

            Helper.cn.Close();
        }
    
//------------------------NAVIGATION BUTTONS------------------------
        private void btnUsers_Click(object sender, EventArgs e)
        {
            Users users = new Users();
            ButtonControl.navigationButton(this, users);
        }

        private void btnMonsters_Click(object sender, EventArgs e)
        {
            Monsters monsters = new Monsters();
            ButtonControl.navigationButton(this, monsters);
        }

        private void btnScores_Click(object sender, EventArgs e)
        {
            Scores scores = new Scores();
            ButtonControl.navigationButton(this, scores);
        }

        private void btnWeapon_Click(object sender, EventArgs e)
        {
            Weapon weapon = new Weapon();
            ButtonControl.navigationButton(this, weapon);
        }
        //------------------------OTHERS------------------------
    }
}
