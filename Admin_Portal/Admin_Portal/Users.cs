﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Portal
{
    public partial class Users : Form
    {
        public Users()
        {
            InitializeComponent();
        }
 //------------------------NAVIGATION BUTTONS------------------------
        private void btnUsers_Click(object sender, EventArgs e)
        {
            Users users = new Users();
            ButtonControl.navigationButton(this, users);
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Form1 main = new Form1();
            ButtonControl.navigationButton(this, main);
        }

        private void btnMonsters_Click(object sender, EventArgs e)
        {
            Monsters monsters = new Monsters();
            ButtonControl.navigationButton(this, monsters);
        }

        private void btnScores_Click(object sender, EventArgs e)
        {
            Scores scores = new Scores();
            ButtonControl.navigationButton(this, scores);
        }

        private void btnWeapon_Click(object sender, EventArgs e)
        {
            Weapon weapon = new Weapon();
            ButtonControl.navigationButton(this, weapon);
        }
 //------------------------OTHER BUTTONS------------------------
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ButtonControl.refreshButton("Players", dgvUsers);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;
            cmd.CommandText = "insert into Players values(@user, @password, 0, 0)";
            cmd.Parameters.AddWithValue("@user", txtAddUsername.Text);
            cmd.Parameters.AddWithValue("@password", txtAddPassword.Text);
            cmd.ExecuteNonQuery();

            Helper.cn.Close();
            MessageBox.Show("Successfully added!");
        }

        private void btnFindUser_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;

            if (txtSearchUser.Text == "Enter User ID")
            {
                MessageBox.Show("No valid User ID found");
                return;
            }

            cmd.CommandText = "Select * from Players where UserID = @userID";
            cmd.Parameters.AddWithValue("@userID", txtSearchUser.Text);
            cmd.ExecuteNonQuery();

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvUsers.DataSource = dt;

            cmd.ExecuteNonQuery();

            cmd.CommandText = "Select UserName from Players where UserID = @userID";
            string userName = (string)cmd.ExecuteScalar();
            txtEditUsername.Text = userName;
            cmd.ExecuteNonQuery();

            cmd.CommandText = "Select Password from Players where UserID = @userID";
            string passwordUser = (string)cmd.ExecuteScalar();
            txtEditPassword.Text = passwordUser;
            cmd.ExecuteNonQuery();

            Helper.cn.Close();

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
             Helper.openConnection();

             SqlCommand cmd = new SqlCommand();
             cmd.Connection = Helper.cn;
             cmd.CommandText = "update Players set UserName = @user,Password = @password where UserID = @userID";
             cmd.Parameters.AddWithValue("@user", txtEditUsername.Text);
             cmd.Parameters.AddWithValue("@password", txtEditPassword.Text);
             cmd.Parameters.AddWithValue("@userID", txtSearchUser.Text);
             cmd.ExecuteNonQuery();

             MessageBox.Show("Sucessfully Updated");
             Helper.cn.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Helper.openConnection();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.cn;

            cmd.CommandText = "delete from Players where UserID = @idno";
            cmd.Parameters.AddWithValue("@idno", dgvUsers.SelectedRows[0].Cells[0].Value.ToString());
            cmd.ExecuteNonQuery();

            Helper.cn.Close();
            MessageBox.Show("Successfully Deleted!");
        }
    }
}
